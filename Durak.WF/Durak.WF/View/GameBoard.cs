﻿using Durak.WF.Presenter;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Durak.WF.View
{
    public partial class GameBoard : Form
    {
        private readonly CardModelPresenter cmp;

        public GameBoard()
        {
            InitializeComponent();

            cmp = new CardModelPresenter(this);

            PictureBox cardToShow = cmp.Deck.Cards[0].CardModel;

            groupBox_GameBoard.Controls.Add(cardToShow);
        }   
    }
}
