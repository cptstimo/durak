﻿using Durak.WF.Model;
using System.Windows.Forms;

namespace Durak.WF.View
{
    public partial class GameMenu : Form
    {
        GameBoard GameBoard;

        public GameMenu()
        {
            InitializeComponent();                     
        }

        private void button_NewGame_Click(object sender, System.EventArgs e)
        {
            this.Hide();
            GameBoard = new GameBoard();
            GameBoard.Show();            
        }

        private void button_Exit_Click(object sender, System.EventArgs e)
        {
            this.Close();
        }
    }
}
