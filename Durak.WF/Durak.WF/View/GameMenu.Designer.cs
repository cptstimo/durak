﻿namespace Durak.WF.View
{
    partial class GameMenu
    {
        /// <summary>S
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GameMenu));
            this.button_NewGame = new System.Windows.Forms.Button();
            this.button_LoadGame = new System.Windows.Forms.Button();
            this.button_Options = new System.Windows.Forms.Button();
            this.button_Exit = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button_NewGame
            // 
            this.button_NewGame.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_NewGame.Location = new System.Drawing.Point(585, 147);
            this.button_NewGame.Name = "button_NewGame";
            this.button_NewGame.Size = new System.Drawing.Size(127, 51);
            this.button_NewGame.TabIndex = 0;
            this.button_NewGame.Text = "New Game";
            this.button_NewGame.UseVisualStyleBackColor = true;
            this.button_NewGame.Click += new System.EventHandler(this.button_NewGame_Click);
            // 
            // button_LoadGame
            // 
            this.button_LoadGame.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_LoadGame.Location = new System.Drawing.Point(585, 214);
            this.button_LoadGame.Name = "button_LoadGame";
            this.button_LoadGame.Size = new System.Drawing.Size(127, 54);
            this.button_LoadGame.TabIndex = 1;
            this.button_LoadGame.Text = "Load  Game";
            this.button_LoadGame.UseVisualStyleBackColor = true;
            // 
            // button_Options
            // 
            this.button_Options.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_Options.Location = new System.Drawing.Point(585, 284);
            this.button_Options.Name = "button_Options";
            this.button_Options.Size = new System.Drawing.Size(127, 51);
            this.button_Options.TabIndex = 3;
            this.button_Options.Text = "Options";
            this.button_Options.UseVisualStyleBackColor = true;
            // 
            // button_Exit
            // 
            this.button_Exit.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_Exit.Location = new System.Drawing.Point(585, 351);
            this.button_Exit.Name = "button_Exit";
            this.button_Exit.Size = new System.Drawing.Size(127, 49);
            this.button_Exit.TabIndex = 4;
            this.button_Exit.Text = "ЭКЗЫТ";
            this.button_Exit.UseVisualStyleBackColor = true;
            this.button_Exit.Click += new System.EventHandler(this.button_Exit_Click);
            // 
            // GameMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1317, 603);
            this.Controls.Add(this.button_Exit);
            this.Controls.Add(this.button_Options);
            this.Controls.Add(this.button_LoadGame);
            this.Controls.Add(this.button_NewGame);
            this.Name = "GameMenu";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button_NewGame;
        private System.Windows.Forms.Button button_LoadGame;
        private System.Windows.Forms.Button button_Options;
        private System.Windows.Forms.Button button_Exit;
    }
}

