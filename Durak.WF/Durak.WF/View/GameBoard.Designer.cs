﻿namespace Durak.WF.View
{
    partial class GameBoard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_SaveGame = new System.Windows.Forms.Button();
            this.button_Surrender = new System.Windows.Forms.Button();
            this.button_ExitGame = new System.Windows.Forms.Button();
            this.groupBox_GameBoard = new System.Windows.Forms.GroupBox();
            this.SuspendLayout();
            // 
            // button_SaveGame
            // 
            this.button_SaveGame.Location = new System.Drawing.Point(1160, 28);
            this.button_SaveGame.Name = "button_SaveGame";
            this.button_SaveGame.Size = new System.Drawing.Size(105, 42);
            this.button_SaveGame.TabIndex = 0;
            this.button_SaveGame.Text = "Save Game";
            this.button_SaveGame.UseVisualStyleBackColor = true;
            // 
            // button_Surrender
            // 
            this.button_Surrender.Location = new System.Drawing.Point(1160, 92);
            this.button_Surrender.Name = "button_Surrender";
            this.button_Surrender.Size = new System.Drawing.Size(105, 41);
            this.button_Surrender.TabIndex = 1;
            this.button_Surrender.Text = "Surrender";
            this.button_Surrender.UseVisualStyleBackColor = true;
            // 
            // button_ExitGame
            // 
            this.button_ExitGame.Location = new System.Drawing.Point(1160, 157);
            this.button_ExitGame.Name = "button_ExitGame";
            this.button_ExitGame.Size = new System.Drawing.Size(105, 40);
            this.button_ExitGame.TabIndex = 2;
            this.button_ExitGame.Text = "Выйды отсюда розбийнык";
            this.button_ExitGame.UseVisualStyleBackColor = true;
            // 
            // groupBox_GameBoard
            // 
            this.groupBox_GameBoard.Location = new System.Drawing.Point(12, 12);
            this.groupBox_GameBoard.Name = "groupBox_GameBoard";
            this.groupBox_GameBoard.Size = new System.Drawing.Size(1131, 580);
            this.groupBox_GameBoard.TabIndex = 3;
            this.groupBox_GameBoard.TabStop = false;
            // 
            // GameBoard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1315, 604);
            this.Controls.Add(this.groupBox_GameBoard);
            this.Controls.Add(this.button_ExitGame);
            this.Controls.Add(this.button_Surrender);
            this.Controls.Add(this.button_SaveGame);
            this.Name = "GameBoard";
            this.Text = "GameBoard";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button_SaveGame;
        private System.Windows.Forms.Button button_Surrender;
        private System.Windows.Forms.Button button_ExitGame;
        private System.Windows.Forms.GroupBox groupBox_GameBoard;
    }
}