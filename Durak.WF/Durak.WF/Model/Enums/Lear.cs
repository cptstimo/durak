﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Durak.WF.Model.Enums
{
    public enum Lear
    {
        Hearts,
        Diamonds,
        Clubs,
        Spades
    }
}
