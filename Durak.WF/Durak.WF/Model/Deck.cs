﻿using Durak.WF.Model.Enums;
using System;
using System.Collections.Generic;

namespace Durak.WF.Model
{
    public class Deck
    {
        private List<Card> _temp;
        public List<Card> Cards { get; set; }

        public Deck()
        {
            _temp = new List<Card>();
            Cards = new List<Card>();            

            Random rand = new Random();       

            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 9; j++)
                {
                    _temp.Add(new Card((Lear)i, (CardValue)j));                   
                }
            }

            for (int i = 35; i >= 0; i--)
            {
                int n = rand.Next(0, i + 1);
                Cards.Add(_temp[n]);
                _temp.RemoveAt(n);
            }
        }
    }
}
