﻿using Durak.WF.Model.Enums;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Durak.WF.Model
{
    public class Card
    {
        public Lear Lear { get; set; }
        public CardValue CardValue { get; set; }
        public PictureBox CardModel { get; set; }

        public Card(Lear lear, CardValue cardValue)
        {
            Lear = lear;
            CardValue = cardValue;

            string CardModelName = lear.ToString() + cardValue.ToString();

            CardModel = new PictureBox();
            CardModel.Image = Image.FromFile(@"D:\Programming\Durak\Durak.WF\Images\" + CardModelName + ".png");           
            CardModel.Size = new System.Drawing.Size(80, 109);
        }
    }
}