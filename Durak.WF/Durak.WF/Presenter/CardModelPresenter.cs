﻿using Durak.WF.Model;
using Durak.WF.View;

namespace Durak.WF.Presenter
{
    public class CardModelPresenter
    {
        public Deck Deck { get; set; }

        private GameBoard _gb_Form;

        public CardModelPresenter(GameBoard gb_Form)
        {
            _gb_Form = gb_Form;
            Deck = new Deck();
        }
    }
}
